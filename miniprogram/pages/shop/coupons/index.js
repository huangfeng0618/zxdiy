const app = getApp()
const db = wx.cloud.database()
const dataPageNum = 6
Page({
  data: {
    color: wx.getExtConfigSync().color,
    myCoupons: ["未使用", "已使用", "已过期"], //优惠卷菜单列表
    color: app.ext.color,
    coupon_list: [],
    status: 0,
    is_all: 0,
    dataNum: 0,
    loadCount: 0,
    showCount: 0,
    length:1
  },
  onLoad: function (options) {
    this.setData({
      loadCount: this.data.loadCount + 1
    })
  },
  onShow: function () {
    this.setData({
      showCount: this.data.showCount + 1
    })
    if (this.data.showCount == this.data.loadCount) {  //判断是首次进入的 不是返回加载的
      this.couponsSearch();
    }else{
      this.onPullDownRefresh();
    }
  },
  _clear() {
    this.setData({
      coupon_list: [],
      is_all: 0,
      dataNum: 0
    })
  },
  couponList(e) {
    let statusTmp = this.data.status;
    if(this.data.status==-1){ //说明是已过期  
      statusTmp = 0;  
    }
    let coupon_list = this.data.coupon_list;
    db.collection('user_coupon').where({ state: statusTmp, openid: wx.getStorageSync('openid'),diyId:wx.getStorageSync('diyId')}).orderBy("createtime", 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
        console.log(res);
        const DATA = res.data;
        this.setData({
          dataNum: this.data.dataNum + dataPageNum
        })
        if (coupon_list.length != this.data.dataNum)   //说明已加载全部
        {
          this.setData({
            is_all: this.data.is_all
          });
        }
        let length = 2;
        if(DATA.length>0)
          length = String(DATA[0].price).length;
        if (e == 1) {  //默认进入 获取首次数据
          coupon_list = DATA;
        } else if (e == 2)  //加载更多
        {
          coupon_list = coupon_list.concat(DATA);
        }
        if(this.data.status==-1){
          let tmpCou = [];
          for(var i=0;i<coupon_list.length;i++){
            if (coupon_list[i].endtime < new Date().getTime()){  //说明已过期了
              tmpCou.push(coupon_list[i])
            }
          }
          this.setData({
            coupon_list: tmpCou,
            length: length
          })
        } else if (this.data.status == 0){
          let tmpCou = [];
          for (var i = 0; i < coupon_list.length; i++) {
            if (coupon_list[i].endtime > new Date().getTime()) {  //说明没有过期了
              tmpCou.push(coupon_list[i])
            }
          }
          this.setData({
            coupon_list: tmpCou,
            length: length
          })
        } else{
          this.setData({
            coupon_list: coupon_list,
            length: length
          })
        }
      })
  },
  // 选择优惠券类型
  couponsSearch(e) {
    const index = e ? e.currentTarget.dataset.index : 0;
    console.log("优惠券"+index)
    this._clear();
    this.setData({
      couponTabIndex: index,
      status: index == 0 ? 0 : index == 1 ? 1 : -1
    });
    this.couponList(1);
  },
  onPullDownRefresh: function () {
    setTimeout(() => {
      this._clear();
      this.couponList(1);
      wx.stopPullDownRefresh();
    }, 500)
  },
  onReachBottom: function () {
    if (!this.data.is_all) {
      this.couponList(2);
    }
  },
  // 使用优惠券
  use() {
    wx.switchTab({
      url: "/pages/com/index/index"
    });
  },
})